import * as Express from "express";
import * as Promise from "bluebird";
import * as Utils from "../../components/Utils";
import {IModule, Authorize, Register} from "../DefaultModule";

@Register
export class StatisticsModule implements IModule {
    public name: string = "statistics";
    public url: string = "/statistics";
    public config: any;
    public db: any;
    public blocktrail: any;

    public init() {};

    public getRoute() {
        let router = Express.Router();

        router.get("/totals", this.getStatistics.bind(this));
        router.get("/transactions", this.getTransactions.bind(this));

        return router;
    }

    private getStatistics(request: any, response: any) {
        let promises = [];

        promises.push(
            this.db.query(`
                SELECT count(\`id\`) as count FROM \`login\`
            `),
            this.db.query(`
                SELECT sum(\`amount\`) as sum
                FROM \`transactions\`
                WHERE \`transaction_type\` = "DEPOSIT" AND \`processed\` = "Y"
            `),
            // Withdrawals
            // ToDo: Make normal - Fix this shit
            this.db.query(`
                SELECT sum(\`amount\`) as sum
                FROM \`transactions\`
                WHERE \`transaction_type\` = "WITHDRAWAL" AND \`processed\` = "Y" AND \`description\` LIKE "Wit%"
            `),
            // Referrals
            // ToDo: Make normal - Fix this shit
            this.db.query(`
                SELECT sum(\`amount\`) as sum
                FROM \`transactions\`
                WHERE \`transaction_type\` = "WITHDRAWAL" AND \`processed\` = "Y" AND \`description\` LIKE "Ref%"
            `)
        );

        Promise.all(promises)
            .then((results:any[]) => {
                Utils.respond.call(response, 200, {
                    members: results[0][0][0]['count'] || 0,
                    deposits: results[1][0][0]['sum'] || 0,
                    withdrawals: results[2][0][0]['sum'] || 0,
                    referrals:  results[3][0][0]['sum'] || 0
                });
            })
            .catch((error) => {
                console.error(error);
                Utils.respond.call(response, 500);
            });
    }

    private getTransactions(request: any, response: any) {
        let promises = [];

        promises.push(
            // Deposits
            this.db.query(`
                SELECT \`process_time\`, \`user_wallet\`, \`amount\`, \`transaction_hash\`
                FROM \`transactions\`
                LEFT JOIN \`wallets\` ON \`transactions\`.\`wallet_id\` = \`wallets\`.\`id\`
                LEFT JOIN \`login\` ON \`wallets\`.\`user_id\` = \`login\`.\`id\`
                WHERE \`process_time\` < now() AND \`processed\` = 'Y' AND \`transaction_type\` = 'DEPOSIT'
                ORDER BY \`process_time\` DESC
                LIMIT 50
            `),
            // Withdrawals
            // ToDo: Make normal - Fix this shit
            this.db.query(`
                SELECT \`process_time\`, \`user_wallet\`, \`amount\`, \`transaction_hash\`
                FROM \`transactions\`
                LEFT JOIN \`login\` ON \`login\`.\`id\` = \`transactions\`.\`user_id\`
                WHERE \`process_time\` < now() AND \`processed\` = 'Y' AND \`transaction_type\` = 'WITHDRAWAL' AND \`description\` LIKE "Wit%"
                ORDER BY \`process_time\` DESC
                LIMIT 50
            `),
            // Referrals
            // ToDo: Make normal - Fix this shit
            this.db.query(`
                SELECT \`process_time\`, \`user_wallet\`, \`amount\`, \`transaction_hash\`
                FROM \`transactions\`
                LEFT JOIN \`login\` ON \`login\`.\`id\` = \`transactions\`.\`user_id\`
                WHERE (\`process_time\` < now() OR \`process_time\` IS NULL) AND \`processed\` = 'Y' AND \`transaction_type\` = 'WITHDRAWAL' AND \`description\` LIKE "Ref%"
                ORDER BY \`process_time\` DESC
                LIMIT 50
            `)
        );

        Promise.all(promises)
            .then((results:any[]) => {
                Utils.respond.call(response, 200, {
                    deposits: results[0][0],
                    withdrawals: results[1][0],
                    referrals:  results[2][0]
                });
            })
            .catch((error) => {
                console.error(error);
                Utils.respond.call(response, 500);
            });
    }
}