import * as Express from "express";
import * as Promise from "bluebird";
import * as Utils from "../../components/Utils";
import * as HttpStatus from "http-status-codes";
import {IModule, Authorize, Register} from "../DefaultModule";
const BlockTrail: any = require('blocktrail-sdk');
const MIN_WITHDRAW = BlockTrail.toSatoshi(0.0001);

@Register
export class LoginModule implements IModule {
    public name: string = "login";
    public url: string = "/user";
    public config: any;
    public db: any;
    public blocktrail: any;

    public init() {};

    public getRoute() {
        let router = Express.Router();

        router.post("/login", this.makeLogin.bind(this));
        router.post("/signup", this.makeSignup.bind(this));
        router.get("/logout", this.makeLogout.bind(this));
        router.get("/check", this.checkUser.bind(this));
        router.get("/stats", this.getUserStats.bind(this));
        router.post("/withdraw", this.makeWithdraw.bind(this));

        return router;
    }

    private makeLogin(request: any, response: any) {

        this.verifyLoginRequest(request)
            .then(body => {
                return this.userByUsername(body.username)
                    .then(user => this.verifyUserLogin(body, user))
                    .then(user => {
                        request.session.login = user;
                        Utils.respond.call(response, 200);
                    })
                    .catch((error: any) => {
                        console.log(error);
                        Utils.respond.call(response, 500, "Internal Server Error");
                    });
            })
            .catch(error => {
                Utils.respond.call(response, 400, error);
            });
    }

    private makeSignup(request: any, response: any) {
        this.verifySignupRequest(request)
            .then(body => {
                this.findWallet(body.wallet)
                    .then(address => this.createUser(body, address))
                    .then(user => {
                        request.session.login = user;
                        Utils.respond.call(response, 200);
                    })
                    .catch((error: any) => {
                        console.log(error);
                        Utils.respond.call(response, 500, "Internal Server Error");
                    });
            })
            .catch(error => {
                Utils.respond.call(response, 400, error);
            });
    }

    @Authorize(["USER", "ADMIN"])
    private getUserStats(request: any, response: any) {
        const userID = request.session["login"].id;
        let promises = [];

        promises.push(
            this.queryUserBalanceSummary(userID),
            // payouts (your last 10 payouts)
            this.db.query(`
                SELECT \`amount\`, \`transaction_hash\`
                FROM \`transactions\`
                WHERE \`transaction_type\` = "WITHDRAWAL" AND \`processed\` = "Y"
                AND \`description\` LIKE "Wit%" AND \`user_id\` = ?
                LIMIT 10
            `, [userID]),
            // last_ref (your 10 commissions)
            this.db.query(`
                SELECT \`amount\`, \`transaction_hash\`, \`username\`
                FROM \`transactions\`
                LEFT JOIN \`login\` ON \`transactions\`.\`user_id\` = \`login\`.\`id\`
                WHERE \`transaction_type\` IN ("WITHDRAWAL","VIRTUAL") AND \`processed\` = 'Y'
                AND \`description\` LIKE "Ref%" AND \`user_id\` = ?
                ORDER BY \`process_time\` DESC
                LIMIT 10
            `, [userID]),
        );

        Promise.all(promises)
            .then((results: any[]) => {
                const summary = results[0];
                let stats = {
                    payouts: results[1][0],
                    last_ref: results[2][0],
                    deposits: summary.investments,
                    withdrawals: summary.withdrawals,
                    referrals: summary.earnings,
                    balance: summary.balance,
                    available_balance: summary.availableBalance,
                    active_deposit: summary.principal,
                };
                Utils.respond.call(response, 200, stats);
            })
            .catch((error) => {
                console.error(error);
                Utils.respond.call(response, 500);
            });
    }

    @Authorize(["USER", "ADMIN"])
    private makeLogout(request: any, response: any) {
        request.session.login = null;
        Utils.respond.call(response, 200, "OK");
    }

    @Authorize(["USER", "ADMIN"])
    private checkUser(request: any, response: any) {
        Utils.respond.call(response, 200, request.session["login"]);
    }

    @Authorize(["USER", "ADMIN"])
    private makeWithdraw(request: any, response: any) {
        Promise.resolve().then(() => {
            // prepare body
            const body = request.body as IWithdrawBody;
            const amount = BlockTrail.toSatoshi(body.amount);

            // validate input
            if (isNaN(amount) || amount < 0)
                return Promise.reject("invalid input");

            // limit by min withdraw value
            if (amount < MIN_WITHDRAW)
                return Promise.reject("amount is less than lower bound");

            const userID = request.session["login"].id;
            return this.queryUserBalanceSummary(userID)
                .then(summary => {
                    // make sure user has enough to withdraw
                    if (amount > summary.availableBalance)
                        return Promise.reject("not enough funds");

                    // calculate how much to withdraw from each account type
                    let fromEarnings = 0;
                    let fromPrincipal = 0;
                    switch (body.account_type) {
                        case "principal":
                            fromPrincipal = amount;
                            break;
                        case "earnings":
                            fromEarnings = amount;
                            break;
                        default:
                            if (amount <= summary.availableEarnings) {
                                fromEarnings = amount;
                            }
                            else {
                                fromEarnings = summary.availableEarnings;
                                fromPrincipal = amount - fromEarnings;
                            }
                            break
                    }

                    // check we have enough to withdraw
                    if (fromPrincipal > summary.availablePrincipal)
                        return Promise.reject("not enough principal");
                    if (fromEarnings > summary.availableEarnings)
                        return Promise.reject("not enough earnings");

                    // create withdraw transactions
                    console.log(`withdraw amount: ${amount} where principal: ${fromPrincipal} earnings: ${fromEarnings}`);
                    this.queryUserWalletWithPlan(userID)
                        .then(info => {
                            let hash: string;
                            let description: string;
                            const values = [];

                            // EARNINGS
                            if (fromEarnings > 0) {
                                let value = (-fromEarnings).toFixed(0);
                                hash = `VIRTUAL_${info.user_wallet}_${userID}_${value}`;
                                description = `Withdrawal EARNINGS by user ${BlockTrail.toBTC(value)} BTC to ${info.user_wallet})`;
                                values.push(`(now(), 'VIRTUAL', 'EARNINGS', '${hash}', 1, '${userID}', '${value}', 0, 'Y', '${description}')`);
                            }

                            // PRINCIPAL
                            if (fromPrincipal > 0) {
                                let value = (-fromPrincipal).toFixed(0);
                                hash = `VIRTUAL_${info.user_wallet}_${userID}_${value}`;
                                description = `Withdrawal PRINCIPAL by user ${BlockTrail.toBTC(value)} BTC to ${info.user_wallet})`;
                                values.push(`(now(), 'VIRTUAL', 'PRINCIPAL', '${hash}', 1, '${userID}', '${value}', 0, 'Y', '${description}')`);
                            }

                            // WITHDRAWAL
                            {
                                let value = amount.toFixed(0);
                                hash = `WITHDRAWAL_${info.user_wallet}_${userID}_${value}`;
                                description = `Withdrawal by user ${BlockTrail.toBTC(value)} BTC to ${info.user_wallet})`;
                                values.push(`(now(), 'WITHDRAWAL', null, '${hash}', 1, '${userID}', '${value}', 0, 'N', '${description}')`);
                            }

                            return this.db.query(`
                                    INSERT INTO \`transactions\`
                                    (\`process_time\`, \`transaction_type\`, \`account_type\`, \`transaction_hash\`, \`payment_id\`, \`user_id\`, \`amount\`, \`confirmations\`, \`processed\`, \`description\`)
                                    VALUES
                                    ${values.join(', ')}
                                `);
                        })
                        .then(() => {
                            console.info(`withdraw of '${amount}BTC' by user '${userID}' scheduled successfully`);
                            this.respond(response, HttpStatus.ACCEPTED)
                        })
                        .catch(e => {
                            console.error("error while creating withdraw transaction", e);
                            this.respond(response, HttpStatus.INTERNAL_SERVER_ERROR);
                        });
                });
        }).catch(e => {
            console.log("withdraw reject", e);
            this.respond(response, HttpStatus.BAD_REQUEST, e);
        })
    }

    private verifyLoginRequest(request): Promise<ILoginBody> {
        const body = request.body as ILoginBody;
        if (!body.username) return Promise.reject("username is empty!");
        if (!body.password) return Promise.reject("password is empty!");
        return Promise.resolve(body);
    }

    private verifySignupRequest(request): Promise<ISignupBody> {
        const body = request.body as ISignupBody;
        if (!body.username) return Promise.reject("username is empty!");
        if (!body.password) return Promise.reject("password is empty!");
        if (!body.email) return Promise.reject("email is empty!");
        if (!body.wallet) return Promise.reject("wallet is empty!");
        return this.userByUsername(body.username)
            .then(user => user ? Promise.reject("user with same username already exist!") : null)
            .then(() => body);
    }

    private findWallet(wallet): Promise<any> {
        return new Promise((resolve, reject) => {
            this.blocktrail.address(wallet, (error, address) => {
                if (error) reject(error);
                else if (!address) reject("address is empty");
                else resolve(address);
            });
        });
    }

    private verifyUserLogin(body: ILoginBody, user: IUser): Promise<IUser> {
        if (!user)
            return Promise.reject("user not exist");

        // TODO: bcrypt
        if (user.password != body.password)
            return Promise.reject("invalid password");

        return Promise.resolve(user);
    }

    private userByUsername(username: string): Promise<IUser> {
        return this.db.query("SELECT * FROM `login` WHERE `username` = ?", [username])
            .then(rows => {
                const row: any[] = rows[0];
                if (row && row.length == 1)
                    return row[0];
            })
    }

    private createUser(body: ISignupBody, address: any): Promise<IUser> {
        const referral = body.referral || null;
        return this.db.query("INSERT INTO `login` (`email`, `username`, `password`, `user_wallet`, `referral`)" +
                             "VALUES (?, ?, ?, ? ,?)",
            [body.email, body.username, body.password, address.address, referral])
            .then(rows => {
                return {
                    id: rows[0].insertId,
                    user_wallet: address.address,
                    points: 0,
                    referral: referral,
                    role: "USER"
                }
            });
    }

    private respond(response, status: number, body?: any) {
        Utils.respond.call(response, status, body);
    }

    private queryUserInvestmentsTotal(userID: string | number, processedOnly: boolean = true): Promise<number> {
        const condition = processedOnly ? `AND \`processed\` = 'Y'` : '';
        const query = `
        SELECT sum(\`amount\`) as sum
        FROM \`transactions\`
        LEFT JOIN \`wallets\` ON \`transactions\`.\`wallet_id\` = \`wallets\`.\`id\`
        LEFT JOIN \`login\` ON \`login\`.\`id\` = \`wallets\`.\`user_id\`
        WHERE \`transaction_type\` = "DEPOSIT"
        AND \`confirmations\` > 0
        ${condition}
        AND \`login\`.\`id\` = ?
        `;
        return this.db.query(query, [userID])
            .then((results: any[]) => Number(results[0][0]['sum']) || 0);
    }

    /**
     * Calculate how much of the deposit money are still frozen and are unavailable to withdraw.
     * @param userID
     * @param timeDelta - how much time money remains frozen after transformation confirmation.
     */
    private queryUserFrozenInvestmentsTotal(userID: string | number, timeDelta: number = 24 * 60 * 60): Promise<number> {
        const query = `
        SELECT sum(\`amount\`) as sum
        FROM \`transactions\`
        LEFT JOIN \`wallets\` ON \`transactions\`.\`wallet_id\` = \`wallets\`.\`id\`
        LEFT JOIN \`login\` ON \`login\`.\`id\` = \`wallets\`.\`user_id\`
        WHERE \`transaction_type\` = "DEPOSIT" AND \`login\`.\`id\` = ?
        AND ((\`transactions\`.\`processed\` != 'Y') OR (\`transactions\`.\`process_time\` > (NOW() - ${timeDelta})))
        `;
        return this.db.query(query, [userID])
            .then((results: any[]) => Number(results[0][0]['sum']) || 0);
    }

    private queryUserEarningsTotal(userID: string | number): Promise<number> {
        // ToDo: Make normal - Fix this shit
        let query = `
        SELECT sum(\`amount\`) as sum
        FROM \`transactions\`
        WHERE ((\`transaction_type\` = "WITHDRAWAL" AND \`description\` LIKE "Ref%")
        OR (\`transaction_type\` = "VIRTUAL"))
        AND \`processed\` = "Y" AND \`user_id\` = ?
        `;
        return this.db.query(query, [userID])
            .then((results: any[]) => Number(results[0][0]['sum']) || 0);
    }

    private queryUserWithdrawalsTotal(userID: string | number): Promise<number> {
        // ToDo: Make normal - Fix this shit
        return this.db.query(`
                SELECT sum(\`amount\`) as sum
                FROM \`transactions\`
                WHERE \`transaction_type\` = "WITHDRAWAL" AND \`processed\` = "Y" AND \`description\` LIKE "Wit%" AND \`user_id\` = ?
            `, [userID])
            .then((results: any[]) => Number(results[0][0]['sum']) || 0);
    }

    public queryUserBalanceSummary(userID: string|number): Promise<IUserBalanceSummary> {
        return this.selectCompactTransactionsByUser(userID).then(transactions => {
            return this.calculateUserAccountSummary(transactions)
        });
    }

    private queryUserWalletWithPlan(userID: string|number): Promise<{
        user_wallet: string,
        transactions: number,
        interval: number,
    }> {
        return this.db.query(`
            SELECT \`user_wallet\`, \`transactions\`, \`interval\`
            FROM \`login\`
            LEFT JOIN \`wallets\` ON \`wallets\`.\`user_id\` = \`login\`.\`id\`
            LEFT JOIN \`plans\` ON \`plans\`.\`id\` = \`wallets\`.\`plan_id\`
            WHERE \`user_id\` = ?
            `, [userID])
            .then(results => results[0][0]);
    }

    private calculateUserAccountSummary(transactions: ICompactTransaction[]): IUserBalanceSummary {
        const hours24 = 24 * 60 * 60 * 1000;
        const unfreezeTime = new Date(Date.now() - hours24);

        let investments = 0;
        let withdrawals = 0;
        let earnings = 0;
        let frozen = 0;
        let earningsWithdrawals = 0;
        let principalWithdrawals = 0;

        let amount: number;
        let processTime: Date;

        for (const transaction of transactions) {
            amount = transaction.amount;
            switch (transaction.transaction_type) {
                case 'DEPOSIT':
                    investments += amount;
                    processTime = new Date(transaction.process_time.getVarDate());
                    if (transaction.processed == 'N' ||
                        processTime > unfreezeTime)
                        frozen += amount;
                    break;
                case 'WITHDRAWAL':
                    withdrawals += amount;
                    break;
                case 'VIRTUAL':
                    if (transaction.processed != 'Y')
                        break;
                    switch (transaction.account_type) {
                        case 'PRINCIPAL':
                            if (amount < 0) principalWithdrawals -= amount;
                            break;
                        default:
                            if (amount > 0) earnings += amount;
                            if (amount < 0) earningsWithdrawals -= amount;
                            break;
                    }
                    break
            }
        }

        const balance = investments + earnings - withdrawals;
        const principal = investments - principalWithdrawals;
        return {
            investments: investments,
            earnings: earnings,

            withdrawals: withdrawals,
            principalWithdrawals: principalWithdrawals,
            earningsWithdrawals: earningsWithdrawals,

            balance: balance,
            frozenInvestments: frozen,
            availableBalance: balance - frozen,
            availableEarnings: earnings - earningsWithdrawals,
            availablePrincipal: principal - frozen,

            principal: principal,
        };
    }

    private selectCompactTransactionsByUser(userID: string|number): Promise<ICompactTransaction[]> {
        return this.db.query(`
            SELECT DISTINCT \`process_time\`, \`transaction_type\`, \`account_type\`, \`amount\`, \`confirmations\`, \`processed\`
            FROM \`transactions\`
            LEFT JOIN \`wallets\` ON \`transactions\`.\`user_id\` = \`wallets\`.\`user_id\`
                                                    OR \`transactions\`.\`wallet_id\` = \`wallets\`.\`id\`
            WHERE \`wallets\`.\`user_id\` = ?
        `, [userID]).then(results => results[0]);
    }
}

interface ILoginBody {
    username;
    password;
}

interface ISignupBody {
    username;
    password;
    email;
    wallet;
    referral;
}

interface IUser {
    id: number;
    user_waller: string;
    points;
    referral;
    role;
    email;
    password;
}

interface IWithdrawBody {
    amount: number;
    account_type: "principal" | "earnings";
}

interface IUserBalanceSummary {
    /** Sum of principal deposits. */
    investments: number;
    /** Sum of any earnings (hourly, refs, etc). */
    earnings: number;

    /** Sum of withdrawals. */
    withdrawals: number;
    /** Sum of principal withdrawals. */
    principalWithdrawals: number;
    /** Sum of earnings withdrawals. */
    earningsWithdrawals: number;

    /** Account balance including frozen investments. */
    balance: number;
    /** Account frozen balance, basically 24h since investment. */
    frozenInvestments: number;
    /** How much user can withdraw in total right now. */
    availableBalance: number;
    /** How much of earnings user can withdraw right now. */
    availableEarnings: number;
    /** How much of principal user can withdraw right now. */
    availablePrincipal: number;

    /** Principal investment, that is used to calculate investment plan. */
    principal: number;
}

interface ICompactTransaction {
    process_time: Date,
    transaction_type: 'DEPOSIT' | 'VIRTUAL' | 'WITHDRAWAL',
    account_type: 'PRINCIPAL' | 'EARNINGS',
    amount: number,
    confirmations: number,
    processed: 'Y' | 'N',
}
