import {Router} from "express";
import * as Utils from "../components/Utils";

export interface IModule {
    name:string;
    url:string;
    db?:any;
    blocktrail?:any;
    config?:any;
    init:() => void;
    getRoute:() => Router;
}

const moduleByType: any = {};
const modules: any[] = [];

export class Module {

    public static moduleOfType<T>(type: new (...args: any[]) => T): T {
        return moduleByType[<any>type];
    }

    public constructor(config:any, database:any, blocktrail:any) {
        let router:any = Router();

        router.get('/', function (req, res) {
            Utils.respond.call(res, 200, {
                api: config.app.title,
                version: config.app.version
            });
        });

        modules.forEach((module:IModule) => {
            module.db = database;
            module.blocktrail = blocktrail;
            module.config = config;
            module.init();
            router.use(module.url, module.getRoute());
        });

        // Otherwise
        router.all("/*", (request:any, resolve:any) => {
            Utils.respond.call(resolve, 404, "Not found");
        });

        return router;
    }
}

export function Register(target:any) {
    let initModule = new target;
    let existModule = modules
        .filter((value) => value.name == initModule.name);

    if (existModule.length) {
        throw new Error(`Module ${target.name} already exists`);
    } else {
        modules.push(initModule);
        moduleByType[target] = initModule;
    }
}

export function Authorize(role:string|string[] = "USER") {
    typeof role == "string" && (role = [<string> role]);
    return (target:any, key:any, descriptor:any) => {
        let method = descriptor.value;
        descriptor.value = function (request:any, response:any) {
            if (request.session && request.session["login"]) {
                if (role.indexOf(request.session["login"].role) >= 0) {
                    method.apply(this, arguments);
                } else {
                    Utils.respond.call(response, 403, "Forbidden");
                }
            } else {
                Utils.respond.call(response, 401, "Not Authorized");
            }
        };
    }
}

export function instance(config:any, database:any, blocktrail:any):any {
    return new Module(config, database, blocktrail);
}

// Loading modules
import "./login/LoginModule";
import "./callback/CallbackModule";
import "./plans/PlansModule";
import "./cron/CronModule";
import "./statistics/StatisticsModule";