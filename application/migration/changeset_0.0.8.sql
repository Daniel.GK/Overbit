ALTER TABLE `wallets`
ADD COLUMN `plan_select` enum('AUTO','MANUAL') NOT NULL DEFAULT 'AUTO' AFTER `wallet`;

REPLACE INTO `settings` (`name`, `value`) VALUES ('version', '0.0.8');
