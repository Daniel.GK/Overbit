import * as _ from "lodash";
import {bookshelf} from "../configs";
import {SchemaTable} from "./schema-table";
const SHALLOW = {shallow: true};

export abstract class BookshelfEntity<T extends BookshelfEntity<T>> extends bookshelf.Model<T> implements IEntity {

    /** Gets or set primary key. */
    ID(value?: number): number { return this.attribute(this.idAttribute, value); }

    /** Name of the table in database. */
    get tableName(): string { return this.tableInfo && this.tableInfo.table; }

    /** Info about model table in database. */
    abstract get tableInfo(): SchemaTable;

    /** Create shallow copy of attributes.
     * @returns {object} */
    shallow() { return this.serialize(SHALLOW); }

    /** Serialize object attributes to pretty JSON format.
     * @param {boolean} [shallow=false] - whether to serialize only attributes without related models.
     * @returns {string} */
    prettyJSON(shallow = false): string {
        return JSON.stringify(shallow ? this.shallow() : this.serialize(), null, ' ');
    }

    /** @private */
    attribute<T>(column: string, value?: T): any {
        if (_.isUndefined(value))
            return this.get(column);
        this.set(column, value);
        return this;
    }
}

interface IEntity {
    /**
     * Gets attribute value.
     * @param column - column name to get.
     */
    attribute<T>(column: string): T;

    /**
     * Sets attribute value.
     * @param column - column name to modify
     * @param value - value to set.
     */
    attribute<T>(column: string, value: T): this;
}
