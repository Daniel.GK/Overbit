import * as knex from "knex";

const config: knex.Config = {
    client: process.env.DB_TYPE || 'mysql',
    pool: {
        min: 0
    },
    connection: {
        host: process.env.DB_HOST || 'localhost',
        user: process.env.DB_USER || 'root2',
        password: process.env.DB_PASS || 'zb1024LV',
        database: process.env.DB_NAME || 'overbit',
        charset: 'utf8'
    },
    migrations: {
        tableName: 'knex'
    }
};

export default knex(config);
