"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var Promise = require("bluebird");
var Express = require("express");
var BlockTrail = require("blocktrail-sdk");
var Utils = require("../../components/Utils");
var DefaultModule_1 = require("../DefaultModule");
var LoginModule = (function () {
    function LoginModule() {
        this.name = "callback";
        this.url = "/callback";
    }
    LoginModule.prototype.init = function () {
        this.blocktrail.getWebhook(this.config.blocktrail.webhook_id, function (error, result) {
            if (error) {
                console.error(error.message);
            }
        });
    };
    ;
    LoginModule.prototype.getRoute = function () {
        var router = Express.Router();
        router.post("/event", this.parseEvent.bind(this));
        return router;
    };
    LoginModule.prototype.parseEvent = function (request, response) {
        var _this = this;
        if (request.body && request.query["secret"] == this.config.blocktrail.apiSecret) {
            fs.appendFileSync("./callback.log", JSON.stringify(request.body) + "\r\n");
            switch (request.body["event_type"]) {
                case "address-transactions":
                    var data_1 = request.body["data"];
                    var addresses = Object.keys(request.body["addresses"]);
                    var input_1 = data_1["inputs"][0];
                    var transactions_1 = [];
                    addresses
                        .forEach(function (wallet) {
                        transactions_1.push(_this.db.query("SELECT `id` FROM `wallets` WHERE `wallet` = ?", [wallet])
                            .then(function (result) {
                            if (result[0] && result[0].length == 1) {
                                return result[0][0]["id"];
                            }
                        })
                            .then(function (wallet_id) {
                            var amount = BlockTrail.toBTC(request.body["addresses"][wallet]);
                            return _this.db.query("\n                                                SELECT `id`\n                                                FROM `transactions`\n                                                WHERE\n                                                    `transaction_hash` = ?\n                                            ", [data_1.hash])
                                .then(function (result) {
                                if (result[0] && result[0].length) {
                                    return _this.db.query("\n                                                        UPDATE `transactions`\n                                                        SET\n                                                            `confirmations` = CASE WHEN `confirmations` > ? THEN `confirmations` ELSE ? END\n                                                        WHERE\n                                                            `transaction_hash` = ?\n                                                    ", [data_1.confirmations, data_1.confirmations, data_1.hash]);
                                }
                                else {
                                    if (wallet_id && request.body["addresses"][wallet] >= 1000000) {
                                        return _this.db.query("\n                                                        INSERT INTO `transactions`\n                                                        SET ?\n                                                        ON DUPLICATE KEY UPDATE\n                                                        confirmations = CASE WHEN confirmations > VALUES(confirmations) THEN confirmations ELSE VALUES(confirmations) END\n                                                    ", {
                                            transaction_type: "DEPOSIT",
                                            transaction_hash: data_1.hash,
                                            amount: request.body["addresses"][wallet],
                                            wallet_id: wallet_id,
                                            confirmations: data_1.confirmations,
                                            description: "Deposit " + amount + " BTC from " + input_1.address + " to " + wallet
                                        });
                                    }
                                    else {
                                        return Promise.resolve();
                                    }
                                }
                            });
                        }));
                    });
                    Promise.all(transactions_1)
                        .then(function () {
                        Utils.respond.call(response, 200, "OK");
                    })
                        .catch(function (error) {
                        console.error(error);
                        Utils.respond.call(response, 500, "Internal Server Error");
                    });
                    break;
                default:
                    Utils.respond.call(response, 405, "Method Not Allowed");
            }
        }
        else {
            Utils.respond.call(response, 400, "Bad Request");
        }
    };
    LoginModule = __decorate([
        DefaultModule_1.Register
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Zhci93d3cvaHRtbC9hcHBsaWNhdGlvbi9tb2R1bGVzL2NhbGxiYWNrL0NhbGxiYWNrTW9kdWxlLnRzIiwic291cmNlcyI6WyIvdmFyL3d3dy9odG1sL2FwcGxpY2F0aW9uL21vZHVsZXMvY2FsbGJhY2svQ2FsbGJhY2tNb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSx1QkFBeUI7QUFDekIsa0NBQW9DO0FBQ3BDLGlDQUFtQztBQUNuQywyQ0FBNkM7QUFDN0MsOENBQWdEO0FBQ2hELGtEQUFtRDtBQUduRDtJQURBO1FBRVcsU0FBSSxHQUFXLFVBQVUsQ0FBQztRQUMxQixRQUFHLEdBQVcsV0FBVyxDQUFDO0lBc0dyQyxDQUFDO0lBakdVLDBCQUFJLEdBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBQyxLQUFLLEVBQUUsTUFBTTtZQUN4RSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNSLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2pDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFBQSxDQUFDO0lBRUssOEJBQVEsR0FBZjtRQUNJLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUU5QixNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBRWxELE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVPLGdDQUFVLEdBQWxCLFVBQW1CLE9BQVksRUFBRSxRQUFhO1FBQTlDLGlCQWdGQztRQS9FRyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUM5RSxFQUFFLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDO1lBRTNFLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxLQUFLLHNCQUFzQjtvQkFDdkIsSUFBSSxNQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDaEMsSUFBSSxTQUFTLEdBQVUsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQzlELElBQUksT0FBSyxHQUFRLE1BQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFbkMsSUFBSSxjQUFZLEdBQUcsRUFBRSxDQUFDO29CQUN0QixTQUFTO3lCQUNKLE9BQU8sQ0FBQyxVQUFDLE1BQWM7d0JBQ3BCLGNBQVksQ0FBQyxJQUFJLENBQ2IsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsK0NBQStDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQzs2QkFDbkUsSUFBSSxDQUFDLFVBQUMsTUFBYTs0QkFHaEIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDckMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDOUIsQ0FBQzt3QkFDTCxDQUFDLENBQUM7NkJBQ0QsSUFBSSxDQUFDLFVBQUMsU0FBa0I7NEJBRXJCLElBQUksTUFBTSxHQUFVLFVBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUN6RSxNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMscVRBS2hCLEVBQUUsQ0FBQyxNQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUNBQ2QsSUFBSSxDQUFDLFVBQUMsTUFBVztnQ0FDZCxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0NBQ2hDLE1BQU0sQ0FBQyxLQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxvZUFNcEIsRUFBRSxDQUFDLE1BQUksQ0FBQyxhQUFhLEVBQUUsTUFBSSxDQUFDLGFBQWEsRUFBRSxNQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQ0FDNUQsQ0FBQztnQ0FBQyxJQUFJLENBQUMsQ0FBQztvQ0FDSixFQUFFLENBQUMsQ0FBQyxTQUFTLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDO3dDQUM1RCxNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdWNBS3hCLEVBQUU7NENBQ0ssZ0JBQWdCLEVBQUUsU0FBUzs0Q0FDM0IsZ0JBQWdCLEVBQUUsTUFBSSxDQUFDLElBQUk7NENBQzNCLE1BQU0sRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE1BQU0sQ0FBQzs0Q0FDekMsU0FBUyxFQUFFLFNBQVM7NENBQ3BCLGFBQWEsRUFBRSxNQUFJLENBQUMsYUFBYTs0Q0FDakMsV0FBVyxFQUFFLGFBQVcsTUFBTSxrQkFBYSxPQUFLLENBQUMsT0FBTyxZQUFPLE1BQVE7eUNBQzFFLENBQUMsQ0FBQztvQ0FDUCxDQUFDO29DQUFDLElBQUksQ0FBQyxDQUFDO3dDQUVKLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7b0NBQzdCLENBQUM7Z0NBQ0wsQ0FBQzs0QkFDTCxDQUFDLENBQUMsQ0FBQzt3QkFDWCxDQUFDLENBQUMsQ0FDVCxDQUFBO29CQUNMLENBQUMsQ0FBQyxDQUFDO29CQUVQLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBWSxDQUFDO3lCQUNwQixJQUFJLENBQUM7d0JBQ0YsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDNUMsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxVQUFDLEtBQUs7d0JBQ1QsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO29CQUMvRCxDQUFDLENBQUMsQ0FBQztvQkFDUCxLQUFLLENBQUM7Z0JBQ1Y7b0JBQ0ksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1lBQ2hFLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3JELENBQUM7SUFDTCxDQUFDO0lBdkdRLFdBQVc7UUFEdkIsd0JBQVE7T0FDSSxXQUFXLENBd0d2QjtJQUFELGtCQUFDO0NBQUEsQUF4R0QsSUF3R0M7QUF4R1ksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBmcyBmcm9tIFwiZnNcIjtcbmltcG9ydCAqIGFzIFByb21pc2UgZnJvbSBcImJsdWViaXJkXCI7XG5pbXBvcnQgKiBhcyBFeHByZXNzIGZyb20gXCJleHByZXNzXCI7XG5pbXBvcnQgKiBhcyBCbG9ja1RyYWlsIGZyb20gXCJibG9ja3RyYWlsLXNka1wiO1xuaW1wb3J0ICogYXMgVXRpbHMgZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvVXRpbHNcIjtcbmltcG9ydCB7SU1vZHVsZSwgUmVnaXN0ZXJ9IGZyb20gXCIuLi9EZWZhdWx0TW9kdWxlXCI7XG5cbkBSZWdpc3RlclxuZXhwb3J0IGNsYXNzIExvZ2luTW9kdWxlIGltcGxlbWVudHMgSU1vZHVsZSB7XG4gICAgcHVibGljIG5hbWU6IHN0cmluZyA9IFwiY2FsbGJhY2tcIjtcbiAgICBwdWJsaWMgdXJsOiBzdHJpbmcgPSBcIi9jYWxsYmFja1wiO1xuICAgIHB1YmxpYyBjb25maWc6IGFueTtcbiAgICBwdWJsaWMgZGI6IGFueTtcbiAgICBwdWJsaWMgYmxvY2t0cmFpbDogYW55O1xuXG4gICAgcHVibGljIGluaXQoKSB7XG4gICAgICAgIHRoaXMuYmxvY2t0cmFpbC5nZXRXZWJob29rKHRoaXMuY29uZmlnLmJsb2NrdHJhaWwud2ViaG9va19pZCwgKGVycm9yLCByZXN1bHQpID0+IHtcbiAgICAgICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBwdWJsaWMgZ2V0Um91dGUoKSB7XG4gICAgICAgIGxldCByb3V0ZXIgPSBFeHByZXNzLlJvdXRlcigpO1xuXG4gICAgICAgIHJvdXRlci5wb3N0KFwiL2V2ZW50XCIsIHRoaXMucGFyc2VFdmVudC5iaW5kKHRoaXMpKTtcblxuICAgICAgICByZXR1cm4gcm91dGVyO1xuICAgIH1cblxuICAgIHByaXZhdGUgcGFyc2VFdmVudChyZXF1ZXN0OiBhbnksIHJlc3BvbnNlOiBhbnkpIHtcbiAgICAgICAgaWYgKHJlcXVlc3QuYm9keSAmJiByZXF1ZXN0LnF1ZXJ5W1wic2VjcmV0XCJdID09IHRoaXMuY29uZmlnLmJsb2NrdHJhaWwuYXBpU2VjcmV0KSB7XG4gICAgICAgICAgICBmcy5hcHBlbmRGaWxlU3luYyhcIi4vY2FsbGJhY2subG9nXCIsIEpTT04uc3RyaW5naWZ5KHJlcXVlc3QuYm9keSkgKyBcIlxcclxcblwiKTtcblxuICAgICAgICAgICAgc3dpdGNoIChyZXF1ZXN0LmJvZHlbXCJldmVudF90eXBlXCJdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSBcImFkZHJlc3MtdHJhbnNhY3Rpb25zXCI6XG4gICAgICAgICAgICAgICAgICAgIGxldCBkYXRhID0gcmVxdWVzdC5ib2R5W1wiZGF0YVwiXTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGFkZHJlc3NlczogYW55W10gPSBPYmplY3Qua2V5cyhyZXF1ZXN0LmJvZHlbXCJhZGRyZXNzZXNcIl0pO1xuICAgICAgICAgICAgICAgICAgICBsZXQgaW5wdXQ6IGFueSA9IGRhdGFbXCJpbnB1dHNcIl1bMF07XG5cbiAgICAgICAgICAgICAgICAgICAgbGV0IHRyYW5zYWN0aW9ucyA9IFtdO1xuICAgICAgICAgICAgICAgICAgICBhZGRyZXNzZXNcbiAgICAgICAgICAgICAgICAgICAgICAgIC5mb3JFYWNoKCh3YWxsZXQ6IHN0cmluZykgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zYWN0aW9ucy5wdXNoKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRiLnF1ZXJ5KFwiU0VMRUNUIGBpZGAgRlJPTSBgd2FsbGV0c2AgV0hFUkUgYHdhbGxldGAgPSA/XCIsIFt3YWxsZXRdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3VsdDogYW55W10pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUb0RvOiBEb2luZyBub3RoaW5nIGlmIHdhbGxldCBpZCBpcyBpbmNvcnJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBDdXJyZW50OiBFeGNlcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0WzBdICYmIHJlc3VsdFswXS5sZW5ndGggPT0gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0WzBdWzBdW1wiaWRcIl07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKCh3YWxsZXRfaWQ/OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUb0RvOiBNYWtlIHJlYWRhYmxlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGFtb3VudCA9ICg8YW55PiBCbG9ja1RyYWlsKS50b0JUQyhyZXF1ZXN0LmJvZHlbXCJhZGRyZXNzZXNcIl1bd2FsbGV0XSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGIucXVlcnkoYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgU0VMRUNUIFxcYGlkXFxgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBGUk9NIFxcYHRyYW5zYWN0aW9uc1xcYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgV0hFUkVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcXGB0cmFuc2FjdGlvbl9oYXNoXFxgID0gP1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBgLCBbZGF0YS5oYXNoXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKHJlc3VsdDogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0WzBdICYmIHJlc3VsdFswXS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFVQREFURSBcXGB0cmFuc2FjdGlvbnNcXGBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgU0VUXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcXGBjb25maXJtYXRpb25zXFxgID0gQ0FTRSBXSEVOIFxcYGNvbmZpcm1hdGlvbnNcXGAgPiA/IFRIRU4gXFxgY29uZmlybWF0aW9uc1xcYCBFTFNFID8gRU5EXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFdIRVJFXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcXGB0cmFuc2FjdGlvbl9oYXNoXFxgID0gP1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGAsIFtkYXRhLmNvbmZpcm1hdGlvbnMsIGRhdGEuY29uZmlybWF0aW9ucywgZGF0YS5oYXNoXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh3YWxsZXRfaWQgJiYgcmVxdWVzdC5ib2R5W1wiYWRkcmVzc2VzXCJdW3dhbGxldF0gPj0gMTAwMDAwMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIElOU0VSVCBJTlRPIFxcYHRyYW5zYWN0aW9uc1xcYFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBTRVQgP1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBPTiBEVVBMSUNBVEUgS0VZIFVQREFURVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25maXJtYXRpb25zID0gQ0FTRSBXSEVOIGNvbmZpcm1hdGlvbnMgPiBWQUxVRVMoY29uZmlybWF0aW9ucykgVEhFTiBjb25maXJtYXRpb25zIEVMU0UgVkFMVUVTKGNvbmZpcm1hdGlvbnMpIEVORFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGAsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zYWN0aW9uX3R5cGU6IFwiREVQT1NJVFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNhY3Rpb25faGFzaDogZGF0YS5oYXNoLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW1vdW50OiByZXF1ZXN0LmJvZHlbXCJhZGRyZXNzZXNcIl1bd2FsbGV0XSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdhbGxldF9pZDogd2FsbGV0X2lkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uZmlybWF0aW9uczogZGF0YS5jb25maXJtYXRpb25zLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGBEZXBvc2l0ICR7YW1vdW50fSBCVEMgZnJvbSAke2lucHV0LmFkZHJlc3N9IHRvICR7d2FsbGV0fWBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gSW52YWxpZCB3YWxsZXQgLT4gSWdub3JlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBQcm9taXNlLmFsbCh0cmFuc2FjdGlvbnMpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgVXRpbHMucmVzcG9uZC5jYWxsKHJlc3BvbnNlLCAyMDAsIFwiT0tcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFV0aWxzLnJlc3BvbmQuY2FsbChyZXNwb25zZSwgNTAwLCBcIkludGVybmFsIFNlcnZlciBFcnJvclwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBVdGlscy5yZXNwb25kLmNhbGwocmVzcG9uc2UsIDQwNSwgXCJNZXRob2QgTm90IEFsbG93ZWRcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBVdGlscy5yZXNwb25kLmNhbGwocmVzcG9uc2UsIDQwMCwgXCJCYWQgUmVxdWVzdFwiKTtcbiAgICAgICAgfVxuICAgIH1cbn0iXX0=