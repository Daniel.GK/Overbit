"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Express = require("express");
var Promise = require("bluebird");
var Utils = require("../../components/Utils");
var DefaultModule_1 = require("../DefaultModule");
var LoginModule_1 = require("../login/LoginModule");
var WalletModule = (function () {
    function WalletModule() {
        this.name = "plans";
        this.url = "/plans";
    }
    WalletModule.prototype.init = function () {
        this.userService = DefaultModule_1.Module.moduleOfType(LoginModule_1.LoginModule);
    };
    ;
    WalletModule.prototype.getRoute = function () {
        var router = Express.Router();
        router.get("/list", this.getPlans.bind(this));
        router.post("/create", this.selectPlan.bind(this));
        return router;
    };
    WalletModule.prototype.getPlans = function (request, response) {
        var user = request.session["login"];
        this.db.query("\n            SELECT `plans`.`id`, `plans`.`name`, `plans`.`percentage`, `plans`.`transactions`, `wallet` FROM `plans`\n            LEFT JOIN `wallets` ON `plans`.`id` = `wallets`.`plan_id` AND `wallets`.`user_id` = ?\n        ", [user.id])
            .then(function (result) {
            Utils.respond.call(response, 200, result[0]);
        })
            .catch(function (error) {
            console.error(error);
            Utils.respond.call(response, 500, "Internal Server Error");
        });
    };
    WalletModule.prototype.selectPlan = function (request, response) {
        var _this = this;
        if (request.body && request.body["plan_id"]) {
            var user_1 = request.session["login"];
            Promise.resolve()
                .then(function () {
                return _this.db.query("\n                    SELECT `plans`.`id`, `plans`.`name`, `plans`.`percentage`, `plans`.`transactions`, `wallet`\n                    FROM `plans`\n                    LEFT JOIN `wallets` ON `plans`.`id` = `wallets`.`plan_id`\n                    WHERE `wallets`.`user_id` = ?\n                    ", [user_1.id]);
            })
                .then(function (result) {
                if (result[0].length == 1 && result[0][0]['wallet'] != null) {
                    return result[0][0];
                }
                else {
                    var newWallet_1 = null;
                    return Promise.resolve()
                        .then(function () {
                        return new Promise(function (resolve, reject) {
                            return _this.blocktrail.initWallet(_this.config.blocktrail.wallet_id, _this.config.blocktrail.wallet_password, function (error, wallet) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve(wallet);
                                }
                            });
                        });
                    })
                        .then(function (wallet) {
                        return new Promise(function (resolve, reject) {
                            wallet.getNewAddress(function (error, new_wallet) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    newWallet_1 = new_wallet;
                                    resolve(new_wallet);
                                }
                            });
                        });
                    })
                        .then(function (new_wallet) {
                        return _this.db.query("INSERT INTO `wallets` (`user_id`, `plan_id`, `wallet`) VALUES (?, ?, ?)", [user_1.id, request.body["plan_id"], new_wallet]);
                    })
                        .then(function () {
                        var respond = result[0][0] || {};
                        respond["wallet"] = newWallet_1;
                        return respond;
                    });
                }
            })
                .then(function (result) {
                Utils.respond.call(response, 200, result);
            })
                .catch(function (error) {
                console.error(error);
                Utils.respond.call(response, 500, "Internal Server Error");
            });
        }
        else {
            Utils.respond.call(response, 400, "Bad Request");
        }
    };
    WalletModule.prototype.updatePlanByWalletUser = function (walletID, userID) {
        var _this = this;
        return this.selectUserPrincipal(userID).then(function (principal) {
            return _this.updateWalletPlan(walletID, principal);
        });
    };
    WalletModule.prototype.selectUserPrincipal = function (userID) {
        return this.userService.queryUserBalanceSummary(userID)
            .then(function (summary) { return summary.principal; });
    };
    WalletModule.prototype.updateWalletPlan = function (walletID, principal) {
        console.log("update wallet plan walletID: " + walletID + " balance: " + principal);
        return this.db.query("\n        UPDATE `wallets`\n        LEFT JOIN `plans` ON (? >= `min_invest` AND (`max_invest` IS NULL OR ? < `max_invest`))\n        SET `wallets`.`plan_id` = `plans`.`id`\n        WHERE `wallets`.`id` = ?\n        ", [principal, principal, walletID]);
    };
    __decorate([
        DefaultModule_1.Authorize(["USER", "ADMIN"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], WalletModule.prototype, "getPlans", null);
    __decorate([
        DefaultModule_1.Authorize(["USER", "ADMIN"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", void 0)
    ], WalletModule.prototype, "selectPlan", null);
    WalletModule = __decorate([
        DefaultModule_1.Register
    ], WalletModule);
    return WalletModule;
}());
exports.WalletModule = WalletModule;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3Zhci93d3cvaHRtbC9hcHBsaWNhdGlvbi9tb2R1bGVzL3BsYW5zL1BsYW5zTW9kdWxlLnRzIiwic291cmNlcyI6WyIvdmFyL3d3dy9odG1sL2FwcGxpY2F0aW9uL21vZHVsZXMvcGxhbnMvUGxhbnNNb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxpQ0FBbUM7QUFDbkMsa0NBQW9DO0FBQ3BDLDhDQUFnRDtBQUNoRCxrREFBc0U7QUFDdEUsb0RBQWlEO0FBR2pEO0lBREE7UUFFVyxTQUFJLEdBQVcsT0FBTyxDQUFDO1FBQ3ZCLFFBQUcsR0FBVyxRQUFRLENBQUM7SUE2SGxDLENBQUM7SUF2SFUsMkJBQUksR0FBWDtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsc0JBQU0sQ0FBQyxZQUFZLENBQUMseUJBQVcsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFBQSxDQUFDO0lBRUssK0JBQVEsR0FBZjtRQUNJLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUU5QixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQzlDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFFbkQsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBR08sK0JBQVEsR0FBaEIsVUFBaUIsT0FBWSxFQUFFLFFBQWE7UUFDeEMsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVwQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxxT0FHYixFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ1IsSUFBSSxDQUFDLFVBQUMsTUFBYTtZQUNoQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2pELENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFDLEtBQVk7WUFDaEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLHVCQUF1QixDQUFDLENBQUM7UUFDL0QsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBR08saUNBQVUsR0FBbEIsVUFBbUIsT0FBWSxFQUFFLFFBQWE7UUFEOUMsaUJBaUVDO1FBL0RHLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxNQUFJLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVwQyxPQUFPLENBQUMsT0FBTyxFQUFFO2lCQUNaLElBQUksQ0FBQztnQkFFRixNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsNlNBS3BCLEVBQUUsQ0FBQyxNQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtZQUNqQixDQUFDLENBQUM7aUJBQ0QsSUFBSSxDQUFDLFVBQUMsTUFBYTtnQkFDaEIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzFELE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBRUosSUFBSSxXQUFTLEdBQUcsSUFBSSxDQUFDO29CQUNyQixNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTt5QkFDbkIsSUFBSSxDQUFDO3dCQUNGLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQVksRUFBRSxNQUFXOzRCQUN6QyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFBRSxVQUFDLEtBQVUsRUFBRSxNQUFXO2dDQUNoSSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29DQUNSLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDbEIsQ0FBQztnQ0FBQyxJQUFJLENBQUMsQ0FBQztvQ0FDSixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Z0NBQ3BCLENBQUM7NEJBQ0wsQ0FBQyxDQUFDLENBQUE7d0JBQ04sQ0FBQyxDQUFDLENBQUM7b0JBQ1AsQ0FBQyxDQUFDO3lCQUNELElBQUksQ0FBQyxVQUFDLE1BQVc7d0JBQ2QsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07NEJBQy9CLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBQyxLQUFVLEVBQUUsVUFBZTtnQ0FDN0MsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQ0FDUixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0NBQ2xCLENBQUM7Z0NBQUMsSUFBSSxDQUFDLENBQUM7b0NBQ0osV0FBUyxHQUFHLFVBQVUsQ0FBQztvQ0FDdkIsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dDQUN4QixDQUFDOzRCQUNMLENBQUMsQ0FBQyxDQUFDO3dCQUNQLENBQUMsQ0FBQyxDQUFBO29CQUNOLENBQUMsQ0FBQzt5QkFDRCxJQUFJLENBQUMsVUFBQyxVQUFlO3dCQUNsQixNQUFNLENBQUMsS0FBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMseUVBQWlGLEVBQUUsQ0FBQyxNQUFJLENBQUMsRUFBRSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQTtvQkFDM0osQ0FBQyxDQUFDO3lCQUNELElBQUksQ0FBQzt3QkFDRixJQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUNqQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsV0FBUyxDQUFDO3dCQUM5QixNQUFNLENBQUMsT0FBTyxDQUFDO29CQUNuQixDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDO1lBQ0wsQ0FBQyxDQUFDO2lCQUNELElBQUksQ0FBQyxVQUFDLE1BQWM7Z0JBQ2pCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDOUMsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxVQUFDLEtBQVU7Z0JBQ2QsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO1lBQy9ELENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNyRCxDQUFDO0lBQ0wsQ0FBQztJQUVNLDZDQUFzQixHQUE3QixVQUE4QixRQUF1QixFQUFFLE1BQXFCO1FBQTVFLGlCQU1DO1FBSkcsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxTQUFTO1lBRWxELE1BQU0sQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVNLDBDQUFtQixHQUExQixVQUEyQixNQUFxQjtRQUM1QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLENBQUM7YUFDbEQsSUFBSSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLFNBQVMsRUFBakIsQ0FBaUIsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFTyx1Q0FBZ0IsR0FBeEIsVUFBeUIsUUFBUSxFQUFFLFNBQVM7UUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBZ0MsUUFBUSxrQkFBYSxTQUFXLENBQUMsQ0FBQztRQUM5RSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMseU5BS3BCLEVBQUUsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQXhHRDtRQURDLHlCQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Ozs7Z0RBZTVCO0lBR0Q7UUFEQyx5QkFBUyxDQUFDLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDOzs7O2tEQWlFNUI7SUF2R1EsWUFBWTtRQUR4Qix3QkFBUTtPQUNJLFlBQVksQ0ErSHhCO0lBQUQsbUJBQUM7Q0FBQSxBQS9IRCxJQStIQztBQS9IWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIEV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCAqIGFzIFByb21pc2UgZnJvbSBcImJsdWViaXJkXCI7XG5pbXBvcnQgKiBhcyBVdGlscyBmcm9tIFwiLi4vLi4vY29tcG9uZW50cy9VdGlsc1wiO1xuaW1wb3J0IHtJTW9kdWxlLCBBdXRob3JpemUsIFJlZ2lzdGVyLCBNb2R1bGV9IGZyb20gXCIuLi9EZWZhdWx0TW9kdWxlXCI7XG5pbXBvcnQge0xvZ2luTW9kdWxlfSBmcm9tIFwiLi4vbG9naW4vTG9naW5Nb2R1bGVcIjtcblxuQFJlZ2lzdGVyXG5leHBvcnQgY2xhc3MgV2FsbGV0TW9kdWxlIGltcGxlbWVudHMgSU1vZHVsZSB7XG4gICAgcHVibGljIG5hbWU6IHN0cmluZyA9IFwicGxhbnNcIjtcbiAgICBwdWJsaWMgdXJsOiBzdHJpbmcgPSBcIi9wbGFuc1wiO1xuICAgIHB1YmxpYyBjb25maWc6IGFueTtcbiAgICBwdWJsaWMgZGI6IGFueTtcbiAgICBwdWJsaWMgYmxvY2t0cmFpbDogYW55O1xuICAgIHB1YmxpYyB1c2VyU2VydmljZTogTG9naW5Nb2R1bGU7XG5cbiAgICBwdWJsaWMgaW5pdCgpIHtcbiAgICAgICAgdGhpcy51c2VyU2VydmljZSA9IE1vZHVsZS5tb2R1bGVPZlR5cGUoTG9naW5Nb2R1bGUpO1xuICAgIH07XG5cbiAgICBwdWJsaWMgZ2V0Um91dGUoKSB7XG4gICAgICAgIGxldCByb3V0ZXIgPSBFeHByZXNzLlJvdXRlcigpO1xuXG4gICAgICAgIHJvdXRlci5nZXQoXCIvbGlzdFwiLCB0aGlzLmdldFBsYW5zLmJpbmQodGhpcykpO1xuICAgICAgICByb3V0ZXIucG9zdChcIi9jcmVhdGVcIiwgdGhpcy5zZWxlY3RQbGFuLmJpbmQodGhpcykpO1xuXG4gICAgICAgIHJldHVybiByb3V0ZXI7XG4gICAgfVxuXG4gICAgQEF1dGhvcml6ZShbXCJVU0VSXCIsIFwiQURNSU5cIl0pXG4gICAgcHJpdmF0ZSBnZXRQbGFucyhyZXF1ZXN0OiBhbnksIHJlc3BvbnNlOiBhbnkpIHtcbiAgICAgICAgbGV0IHVzZXIgPSByZXF1ZXN0LnNlc3Npb25bXCJsb2dpblwiXTtcblxuICAgICAgICB0aGlzLmRiLnF1ZXJ5KGBcbiAgICAgICAgICAgIFNFTEVDVCBcXGBwbGFuc1xcYC5cXGBpZFxcYCwgXFxgcGxhbnNcXGAuXFxgbmFtZVxcYCwgXFxgcGxhbnNcXGAuXFxgcGVyY2VudGFnZVxcYCwgXFxgcGxhbnNcXGAuXFxgdHJhbnNhY3Rpb25zXFxgLCBcXGB3YWxsZXRcXGAgRlJPTSBcXGBwbGFuc1xcYFxuICAgICAgICAgICAgTEVGVCBKT0lOIFxcYHdhbGxldHNcXGAgT04gXFxgcGxhbnNcXGAuXFxgaWRcXGAgPSBcXGB3YWxsZXRzXFxgLlxcYHBsYW5faWRcXGAgQU5EIFxcYHdhbGxldHNcXGAuXFxgdXNlcl9pZFxcYCA9ID9cbiAgICAgICAgYCwgW3VzZXIuaWRdKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3VsdDogYW55W10pID0+IHtcbiAgICAgICAgICAgICAgICBVdGlscy5yZXNwb25kLmNhbGwocmVzcG9uc2UsIDIwMCwgcmVzdWx0WzBdKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goKGVycm9yOiBFcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIFV0aWxzLnJlc3BvbmQuY2FsbChyZXNwb25zZSwgNTAwLCBcIkludGVybmFsIFNlcnZlciBFcnJvclwiKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIEBBdXRob3JpemUoW1wiVVNFUlwiLCBcIkFETUlOXCJdKVxuICAgIHByaXZhdGUgc2VsZWN0UGxhbihyZXF1ZXN0OiBhbnksIHJlc3BvbnNlOiBhbnkpIHtcbiAgICAgICAgaWYgKHJlcXVlc3QuYm9keSAmJiByZXF1ZXN0LmJvZHlbXCJwbGFuX2lkXCJdKSB7XG4gICAgICAgICAgICBsZXQgdXNlciA9IHJlcXVlc3Quc2Vzc2lvbltcImxvZ2luXCJdO1xuXG4gICAgICAgICAgICBQcm9taXNlLnJlc29sdmUoKVxuICAgICAgICAgICAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gR2V0IHBsYW4gaWYgZXhpc3RzXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmRiLnF1ZXJ5KGBcbiAgICAgICAgICAgICAgICAgICAgU0VMRUNUIFxcYHBsYW5zXFxgLlxcYGlkXFxgLCBcXGBwbGFuc1xcYC5cXGBuYW1lXFxgLCBcXGBwbGFuc1xcYC5cXGBwZXJjZW50YWdlXFxgLCBcXGBwbGFuc1xcYC5cXGB0cmFuc2FjdGlvbnNcXGAsIFxcYHdhbGxldFxcYFxuICAgICAgICAgICAgICAgICAgICBGUk9NIFxcYHBsYW5zXFxgXG4gICAgICAgICAgICAgICAgICAgIExFRlQgSk9JTiBcXGB3YWxsZXRzXFxgIE9OIFxcYHBsYW5zXFxgLlxcYGlkXFxgID0gXFxgd2FsbGV0c1xcYC5cXGBwbGFuX2lkXFxgXG4gICAgICAgICAgICAgICAgICAgIFdIRVJFIFxcYHdhbGxldHNcXGAuXFxgdXNlcl9pZFxcYCA9ID9cbiAgICAgICAgICAgICAgICAgICAgYCwgW3VzZXIuaWRdKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3VsdDogYW55W10pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdFswXS5sZW5ndGggPT0gMSAmJiByZXN1bHRbMF1bMF1bJ3dhbGxldCddICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHRbMF1bMF07XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBDcmVhdGUgd2FsbGV0XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgbmV3V2FsbGV0ID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlOiBhbnksIHJlamVjdDogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ibG9ja3RyYWlsLmluaXRXYWxsZXQodGhpcy5jb25maWcuYmxvY2t0cmFpbC53YWxsZXRfaWQsIHRoaXMuY29uZmlnLmJsb2NrdHJhaWwud2FsbGV0X3Bhc3N3b3JkLCAoZXJyb3I6IGFueSwgd2FsbGV0OiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHdhbGxldCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhlbigod2FsbGV0OiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdhbGxldC5nZXROZXdBZGRyZXNzKChlcnJvcjogYW55LCBuZXdfd2FsbGV0OiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXdXYWxsZXQgPSBuZXdfd2FsbGV0O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG5ld193YWxsZXQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKG5ld193YWxsZXQ6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShgSU5TRVJUIElOVE8gXFxgd2FsbGV0c1xcYCAoXFxgdXNlcl9pZFxcYCwgXFxgcGxhbl9pZFxcYCwgXFxgd2FsbGV0XFxgKSBWQUxVRVMgKD8sID8sID8pYCwgW3VzZXIuaWQsIHJlcXVlc3QuYm9keVtcInBsYW5faWRcIl0sIG5ld193YWxsZXRdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgcmVzcG9uZCA9IHJlc3VsdFswXVswXSB8fCB7fTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uZFtcIndhbGxldFwiXSA9IG5ld1dhbGxldDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbmQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC50aGVuKChyZXN1bHQ6IHN0cmluZykgPT4ge1xuICAgICAgICAgICAgICAgICAgICBVdGlscy5yZXNwb25kLmNhbGwocmVzcG9uc2UsIDIwMCwgcmVzdWx0KTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyb3I6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgVXRpbHMucmVzcG9uZC5jYWxsKHJlc3BvbnNlLCA1MDAsIFwiSW50ZXJuYWwgU2VydmVyIEVycm9yXCIpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgVXRpbHMucmVzcG9uZC5jYWxsKHJlc3BvbnNlLCA0MDAsIFwiQmFkIFJlcXVlc3RcIik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgdXBkYXRlUGxhbkJ5V2FsbGV0VXNlcih3YWxsZXRJRDogbnVtYmVyfHN0cmluZywgdXNlcklEOiBudW1iZXJ8c3RyaW5nKSB7XG4gICAgICAgIC8vIGNhbGN1bGF0ZSBwcmluY2lwYWwgaW52ZXN0bWVudCBvZiBwcm92aWRlZCB1c2VyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdFVzZXJQcmluY2lwYWwodXNlcklEKS50aGVuKHByaW5jaXBhbCA9PiB7XG4gICAgICAgICAgICAvLyBmb3Igd2FsbGV0IHVwZGF0ZSBwbGFuIGRlcGVuZGluZyBvbiBhY3RpdmUgcHJpbmNpcGFsXG4gICAgICAgICAgICByZXR1cm4gdGhpcy51cGRhdGVXYWxsZXRQbGFuKHdhbGxldElELCBwcmluY2lwYWwpO1xuICAgICAgICB9KVxuICAgIH1cblxuICAgIHB1YmxpYyBzZWxlY3RVc2VyUHJpbmNpcGFsKHVzZXJJRDogc3RyaW5nfG51bWJlcik6IFByb21pc2U8bnVtYmVyPiB7XG4gICAgICAgIHJldHVybiB0aGlzLnVzZXJTZXJ2aWNlLnF1ZXJ5VXNlckJhbGFuY2VTdW1tYXJ5KHVzZXJJRClcbiAgICAgICAgICAgIC50aGVuKHN1bW1hcnkgPT4gc3VtbWFyeS5wcmluY2lwYWwpO1xuICAgIH1cblxuICAgIHByaXZhdGUgdXBkYXRlV2FsbGV0UGxhbih3YWxsZXRJRCwgcHJpbmNpcGFsKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGB1cGRhdGUgd2FsbGV0IHBsYW4gd2FsbGV0SUQ6ICR7d2FsbGV0SUR9IGJhbGFuY2U6ICR7cHJpbmNpcGFsfWApO1xuICAgICAgICByZXR1cm4gdGhpcy5kYi5xdWVyeShgXG4gICAgICAgIFVQREFURSBcXGB3YWxsZXRzXFxgXG4gICAgICAgIExFRlQgSk9JTiBcXGBwbGFuc1xcYCBPTiAoPyA+PSBcXGBtaW5faW52ZXN0XFxgIEFORCAoXFxgbWF4X2ludmVzdFxcYCBJUyBOVUxMIE9SID8gPCBcXGBtYXhfaW52ZXN0XFxgKSlcbiAgICAgICAgU0VUIFxcYHdhbGxldHNcXGAuXFxgcGxhbl9pZFxcYCA9IFxcYHBsYW5zXFxgLlxcYGlkXFxgXG4gICAgICAgIFdIRVJFIFxcYHdhbGxldHNcXGAuXFxgaWRcXGAgPSA/XG4gICAgICAgIGAsIFtwcmluY2lwYWwsIHByaW5jaXBhbCwgd2FsbGV0SURdKTtcbiAgICB9XG59XG4iXX0=